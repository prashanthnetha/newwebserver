module "user1webserver" {
  source           = "github.com/prashanth438/terraformwebserver.git"
  region           = var.region
  key_name         = var.key_name
  ami              = var.ami
  instance_type    = var.instance_type
  private_key_path = var.private_key_path

}